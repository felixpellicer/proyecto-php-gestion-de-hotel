## Gestión de un hotel mediante PHP

**Descripción (Finalización de estancias)**
Este programa permite completar los datos de una estancia cuando los huéspedes marchan del hotel. El programa pedirá el número de habitación de la estancia. Después, mostrará los datos de la estancia (fecha de entrada, y datos de las personas que ocupaban la habitación). Se completarán los datos en los campos de la tabla de Stays que faltan: la fecha de salida (que se puede copiar de la reserva), y el método de pago (que tiene que introducir el usuario). Se marcará la habitación como disponible.


**Preparando el entorno de trabajo:**
Deberás tener instalado Vagrant en la <a href="https://releases.hashicorp.com/vagrant/2.3.3/">versión 2.3.3</a>, una vez hayas clonado el proyecto realiza la instalación:<br>

<b>En Linux</b> en el siguiente <a href="https://developer.hashicorp.com/vagrant/install?product_intent=vagrant#linux">enlace</a> siguiendo las instrucciones según tu distribución.<br>
<b>En Windows</b> en el siguiente <a href="https://developer.hashicorp.com/vagrant/install?product_intent=vagrant#windows">enlace</a> descargando el ejecutable.<br>

Una vez instalado Vagrant y clonado el proyecto, abre una terminal en el directorio donde se encuentra el archivo Vagrantfile (M9UF1\activitats)y levanta la máquina virtual con el siguiente comando:<br>
<code>vagrant up</code><br>

<b>Instrucciones:</b><br>
Cuando la máquina virtual esté creada desde la misma terminal de comandos nos podremos conectar a la máquina mediante SSH y con el uso del siguiente comando:<br>
<code>vagrant ssh</code><br>

Con esto tendremos acceso a la base de datos del hotel y podremos configurar parámetros del sistema.Para utilizar un gestor de bases de datos, como Dbeaver, primero crearemos un usuario y contrasña con privilegios para podernos conectar y realizar cambios y consultas:<br>
<code>sudo mysql</code><br>
<code>use mysql</code><br>
<code>select user, host from user;</code><br>
<code>create user 'username'@'%' identified by 'password';</code><br>
<code>select user, host from user;</code><br>
<code>grant all privileges on *.* to 'username'@'%';</code>

<b>Todos los archivos del proyecto se guardan en el directorio “M9UF1\activitats\www” para visualizar-los desde el navegador ingresamos la dirección IP de la máquina en el buscador (https://192.168.56.12) y accederemos al índice principal.</b>

Para apagar la máquina virtual, primero sal de la conexión SSH con un <code>exit</code> y una vez situado en el directorio del proyecto utiliza el comando -> <code>vagrant halt</code>. Toda la guia de comandos de Vagrant en este <a href="https://www.busindre.com/guia_rapida_de_vagrant">enlace</a>.

<b>Que contiene el archivo Vagrantfile?</b><br>
Contiene la configuración para la máquina virtual en lenguaje Ruby.<br>
<code>Vagrant.configure("2") do |config|</code> Especifica que se está usando la versión de Vagrant 2.

<code>config.vm.box = "debian/bullseye64"</code> Se crea el contenedor de la máquina basada en Debian Bullseye x64.

<code> config.vm.hostname = "webserver"</code> Establece el hostname "webserver" a la máquina virtual.

<code>config.vm.define "webserver"</code> Define un nombre interno para la máquina virtual dentro del entorno de Vagrant. Importante sobretodo cuando se usan múltiples máquinas.

<code> config.vm.network "private_network", ip: "192.168.56.12"</code> Este parámetro configura y asigna una dirección de red privada para permitir que sea accesible solo desde el host y otras máquinas virtuales en la misma red privada.

<code>  config.vm.provision "shell", path: "bootstrap.sh"</code>
Esta línea configura un aprovisionador de shell que ejecutará el script especificado por la ruta "bootstrap.sh" dentro de la máquina virtual. Esto es útil para realizar configuraciones adicionales y automatizar tareas de instalación. <a href="https://gitlab.com/felixpellicer/proyecto-php-gestion-de-hotel/-/blob/main/activitats/bootstrap.sh?ref_type=heads">Aquí se automatiza la configuración del entorno de servidor web con base de datos y soporte para PHP de este proyecto.</a>

<code>config.vm.provider "virtualbox" do |vb|</code> Esta línea inicia un bloque de configuración específico para el proveedor VirtualBox. Esto permite definir configuraciones específicas para VirtualBox.

<code>vb.memory = "512"</code> Dentro del bloque de configuración de VirtualBox, esta línea asigna 512 MB de memoria RAM a la máquina virtual.

<code>vb.cpus = 1</code> También dentro del bloque de configuración de VirtualBox, esta línea asigna un solo CPU a la máquina virtual.
<hr><br>
<p>This work is licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International</a></p>

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Cc-by-nc-sa_icon.svg/2560px-Cc-by-nc-sa_icon.svg.png" width="88" height="31">