<?php

require_once 'Connection.php';

session_start();

function show_messages() {
    if (isset($_SESSION['error'])) {
      echo "<div class='alert alert-danger' role='alert'>{$_SESSION['error']}</div>";
      unset($_SESSION['error']);
    }
    if (isset($_SESSION['success'])) {
      echo "<div class='alert alert-success' role='alert'>{$_SESSION['success']}</div>";
      unset($_SESSION['success']);
    }
  }

// Función para actualizar la estancia. Le paso los parametros necesarios.
function updateStay($conn, $Id, $checkOut, $paymentType, $paymentDateTime) {
    try {
        // Limpiar los datos del formulario
        $paymentType = trim($paymentType);
        // Verifica que el PaymentType sea 'cash' o 'card'
        if ($paymentType !== 'Cash' && $paymentType !== 'Card') {
            throw new Exception("El tipo de pago debe ser 'Cash' o 'Card'.");
        }
        
        $statement = $conn->prepare("UPDATE Stays SET CheckOut = :checkOut , PaymentType = :paymentType, PaymentDateTime = :paymentDateTime WHERE Id = :Id");
        $statement->bindParam(':Id', $Id);
        $statement->bindParam(':checkOut', $checkOut);
        $statement->bindParam(':paymentType', $paymentType);
        $statement->bindParam(':paymentDateTime', $paymentDateTime);
        
        $statement->execute();

        // Verificar el número de filas afectadas para confirmar que la actualización fue exitosa
        $rowCount = $statement->rowCount();
        if ($rowCount > 0) {
            // Establece un mensaje de éxito en la sesión
            $_SESSION['success'] = "Estancia cerrada correctamente.";
            return true; // La actualización fue exitosa
        } else {
            throw new Exception("Error al actualizar la estancia.");
        }
    } catch (Exception $e) {
        // Manejo de errores si ocurre algún problema durante la actualización
        $_SESSION['error'] = "Error al actualizar la estancia: " . $e->getMessage();
        return false;
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Recoge los datos enviados por el formulario
    $Id = $_POST['Id'];
    $CheckIn = $_POST['CheckIn'];
    $RoomNumber = $_POST['RoomNumber'];
    $CheckOut = $_POST['CheckOut'];
    $FirstName = $_POST['FirstName'];
    $LastName = $_POST['LastName'];
    $Nationality = $_POST['Nationality'];
    $PhoneNumber = $_POST['PhoneNumber'];
    $TotalPrice = $_POST['TotalPrice'];
    $PaymentType = $_POST['PaymentType'];
    $PaymentDateTime = $_POST['PaymentDateTime'];

    $conn = connect();

    //Llamo a la funcion y le paso las variables que necesita
    updateStay($conn, $Id, $CheckOut, $PaymentType, $PaymentDateTime);

    header('Location: index.php');
    exit();
} else {
    header('Location: index.php');
    exit();
}
?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Proyecto final</title>
  </head>
  <body> 
  <?php show_messages();?> 
    <main role="main" class="container">
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
