<?php

require_once 'Connection.php';

session_start();

#Funcion para comprobar que la habitación introducida existe haciendo consulta con el numero de habitación
#Con la función sizeoff se mira que haya resultado
function checkRoom($conn, $numroom) {
  $statement = $conn->prepare("SELECT RoomNumber FROM Rooms WHERE RoomNumber=:numroom");
  $statement->bindParam(':numroom', $numroom);
  $statement->execute();
  $result = $statement->fetchAll();
  
  if (sizeof($result)==0) {
      throw new Exception("L'habitació introduïda no existeix.");
  }

  return true;
}

#Obtengo la estancia con esta función y la guardo la estancia en un array. Obengo la fecha de CheckOut de la 
#tabla de Bookings y información del cliente en la tabla Customers usando JOINS.
function getStays($conn, $numroom, $PaymentType){
  $statement = $conn->prepare("SELECT s.Id, s.CheckIn, s.RoomNumber, bs.CheckOut, c.FirstName, c.LastName, c.Nationality, c.PhoneNumber, s.TotalPrice, s.PaymentType, s.PaymentDateTime
  FROM Stays s
  JOIN Bookings bs ON s.BookingId=bs.Id
  JOIN Customers c ON bs.CustomerId=c.Id
  WHERE PaymentType IS NULL AND RoomNumber =:numroom");
  $statement->bindParam(':numroom', $numroom);
  $statement->execute();
  $stays = $statement->fetchAll();
  
  return $stays;
}

#Muestro el contenido del array en formato de tablas
function showStays($conn, $stays, $PaymentType) {
  $currentDateTime = date('Y-m-d H:i:s'); //creo una variable con la función date para tener la fecha actual
  if (sizeof($stays)==0) {
    throw new Exception("No hi ha cap estada oberta en aquesta habitació.");
    //echo "<p>No hi ha cap estada oberta en aquesta habitació.</p>\n";
  } else {
    ?>
    <main role="main" class="container">
    <h1 class="mt-5">Historial d'estades</h1>
    <table class='table table-striped'>
    <tr>
        <th>Id</th>
        <th>CheckIn</th>
        <th>RoomNumber</th>
        <th>CheckOut</th>
        <th>FirstName</th>
        <th>LastName</th>
        <th>Nationality</th>
        <th>PhoneNumber</th>
        <th>TotalPrice</th>
        <th>PaymentType</th>
        <th>PaymentDateTime</th>
        <th>Actions</th>
      </tr>
    <?php
    foreach ($stays as $stay) {
      echo "<tr>";
      echo "<td>{$stay['Id']}</td>";
      echo "<td>{$stay['CheckIn']}</td>";
      echo "<td>{$stay['RoomNumber']}</td>";
      echo "<td>{$stay['CheckOut']}</td>";
      echo "<td>{$stay['FirstName']}</td>"; 
      echo "<td>{$stay['LastName']}</td>";
      echo "<td>{$stay['Nationality']}</td>";
      echo "<td>{$stay['PhoneNumber']}</td>";
      echo "<td>{$stay['TotalPrice']}</td>";
      echo "<td>{$PaymentType}</td>"; // Tipo de pago obtenido de la pagina principal
      echo "<td>{$currentDateTime}</td>"; // Fecha actual a través de la función
      echo "<td>"; // Abre una nueva celda para los botones de acción
      echo "<form method='post' action='update.php'>";
      foreach ($stay as $key => $value) {
          if ($key !== 'PaymentType' && $key !== 'PaymentDateTime') {
              echo "<input type='hidden' name='$key' value='$value'>";
          }
      }
      echo "<input type='hidden' name='PaymentType' value='{$PaymentType}'>"; // Campo oculto para PaymentType
      echo "<input type='hidden' name='PaymentDateTime' value='{$currentDateTime}'>"; // Campo oculto para PaymentDateTime
      echo "<button type='submit' class='btn btn-primary'>Tancar Estada</button>";
      echo "</form>";
      echo "</td>";
      echo "</tr>";
    }
    ?>
    </table>
    <?php
  }
}


function show_messages() {
  if (isset($_SESSION['error'])) {
    echo "<div class='alert alert-danger' role='alert'>{$_SESSION['error']}</div>";
    unset($_SESSION['error']);
  }
  if (isset($_SESSION['success'])) {
    echo "<div class='alert alert-success' role='alert'>{$_SESSION['success']}</div>";
    unset($_SESSION['success']);
  }
}

try {
  $conn = connect();  // Asegurarse de que esta función está definida y devuelve una conexión válida

  // Comprobar que se ha introducido el número de habitación
  if (!isset($_POST['numroom']) || empty($_POST['numroom'])) {
    throw new Exception("Falten paràmetres.");
  }

  //Compruebo que se ha elegido un metodo de pago
  if (!isset($_POST['PaymentType']) || empty($_POST['PaymentType'])) {
    throw new Exception("Falten paràmetres.");
  }

  $PaymentType = $_POST['PaymentType'];

  // Validar que el valor introducido sea de tipo numérico entero y quitar espacios en blanco
  $numroom = filter_var(trim($_POST['numroom']), FILTER_VALIDATE_INT);
  if ($numroom === false) {
    throw new Exception("El contingut d'aquest camp ha de ser de valor numèric.");
  }

  // Llamar a la función para comprobar que la habitación existe
  $validatedRoomNumber = checkRoom($conn, $numroom);

  // Si la habitación existe, mostrar las estancias para cerrar
  if ($validatedRoomNumber) {
    $stays = getStays($conn, $numroom, $PaymentType); 
    showStays($conn, $stays, $PaymentType);
  }
} catch (Exception $e) {
  $_SESSION['error'] = $e->getMessage();
  header('Location: index.php');
  exit();
}
?>

<!DOCTYPE html>
<html lang="ca">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Proyecto final</title>
  </head>
  <body> 
  <?php show_messages(); ?> 
    <main role="main" class="container">
    </main>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
